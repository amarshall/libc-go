module modernc.org/libc

go 1.18

require (
	github.com/dustin/go-humanize v1.0.1
	github.com/google/uuid v1.3.0
	github.com/mattn/go-isatty v0.0.16
	golang.org/x/sys v0.9.0
	modernc.org/mathutil v1.5.0
	modernc.org/memory v1.6.0
)

require (
	github.com/kballard/go-shellquote v0.0.0-20180428030007-95032a82bc51 // indirect
	github.com/remyoudompheng/bigfft v0.0.0-20230129092748-24d4a6f8daec // indirect
	golang.org/x/mod v0.11.0 // indirect
	golang.org/x/tools v0.10.0 // indirect
	lukechampine.com/uint128 v1.2.0 // indirect
	modernc.org/cc/v3 v3.41.0 // indirect
	modernc.org/cc/v4 v4.8.2 // indirect
	modernc.org/ccgo/v3 v3.16.14 // indirect
	modernc.org/ccgo/v4 v4.0.0-20230706200502-56f29b3f5b85 // indirect
	modernc.org/ccorpus2 v1.4.0 // indirect
	modernc.org/gc/v2 v2.3.0 // indirect
	modernc.org/opt v0.1.3 // indirect
	modernc.org/strutil v1.1.3 // indirect
	modernc.org/token v1.1.0 // indirect
)
